#!/usr/bin/python3
'''
Basic ADTs
  List (array) based
    Stack - is_empty, size, push, pop, peek
    Queue - is_empty, size, enqueue, dequeue

  Pointer based
    LinkedList - is_empty, size, clear, insert, add, get, set, remove, to_list
'''

__all__     = ['a', 'b', 'c']
__version__ = '0.1'
__author__  = 'fmc'
__doc__     = "Basic ADTs created by fmc (Stack, Queue, LinkedList)"


class Stack:

    def __init__(self):
        self.coses = []

    def is_empty(self):
        return self.coses == []

    def push(self, cosa):
        self.coses.append(cosa)

    def pop(self):
        return self.coses.pop()

    def peek(self):
        return self.coses[len(self.items)-1]

    def size(self):
        return len(self.coses)


class Queue:

    def __init__(self):
        self.coses = []

    def is_empty(self):
        return self.coses == []

    def size(self):
        return len(self.coses)

    def enqueue(self, cosa):
        self.coses.append(cosa)

    def dequeue(self):
        return self.coses.pop(0)


class ListNode:

    def __init__(self, cosa = None):
        self._value = cosa
        self._next = None

    def get_value(self):
        return self._value


class LinkedList:
    ''' LinkedList ADT'''

    def __init__(self):
        self._front = None
        self._lastnode = None
        self._nodes = 0
        self._next_node = None
        self._next_end = False

    def __len__(self):
        return self.size()

    def __iter__(self):           ## Generator
        ''' Generator thar returns the value of a node'''
        print("Called __iter__ (generator)")
        current_node = self._front
        while current_node is not None:
            yield current_node._value
            current_node = current_node._next

    def __next__(self):
        if self._next_end:
            raise StopIteration

        # Check for empty list...
        if self._next_node == None:
            if self._front is not None:
                print("   Assignat front...")
                self._next_node = self._front
            else:
                print("   No hi ha nodes...")
                raise StopIteration
        else:
            self._next_node = self._next_node._next
            if self._next_node._next == None:
                self._next_end = True
                print("   End of list...")


        return self._next_node

    def __getitem__(self, k):
        ''' Returns the value of a given list node'''
        print(" *** Geting: " + str(k))
        current_node = self._front
        for i in range(k):
            if i == self._nodes - 1:
                raise IndexError
            current_node = current_node._next
        return current_node._value

    def clear(self):
        ''' Clears the nodes os the LinkedLst'''
        pass

    def is_empty(self):
        ''' Check (boolan value) whether the list is empty '''
        return self._nodes == 0

    def size(self):
        ''' Return the number of _nodes in the list'''
        return self._nodes

    def insert(self, where, cosa):
        ''' Inserts a node in a given position'''
        if where < 0 or where > self._nodes + 1:
            print("Location out of bounds...")
        else:
            node = ListNode(cosa)
            if where == 0:
                node._next = self._front
                self._front = node
            else:
                if where == self._nodes + 1:
                    self.add(cosa)
                else:
                    current_node = self._front
                    for i in range(where -1):
                        current_node = current_node._next
                    node._next = current_node._next
                    current_node._next = node
            self._nodes += 1

    def add(self, cosa):
        ''' Adds a node to the end of the list'''
        node = ListNode(cosa)
        # if first node...
        if self._front == None:
            self._front = node
            self._nodes += 1
            self._lastnode = node
        else:
            # removing loop via informing lastnode. Now add() is O(1).
            # current_node = self._front
            # while current_node._next != None:
            #     current_node = current_node._next
            # current_node._next = node
            self._lastnode._next = node
            self._lastnode = node
            self._nodes += 1

    def get(self, which):
        ''' Gets the value of a given (which) node'''
        if which < 0 or which > self._nodes:
            print("Location out of bounds...")
        else:
            current_node = self._front
            for i in range(which-1):
                current_node = current_node._next
            return current_node._value

    def set(self, which, cosa):
        ''' Sets de value of an existing (where) node'''
        if which < 0 or which > self._nodes:
            print("Location out of bounds...")
        else:
            if which == 0:
                self._front._value = cosa
            else:
                current_node = self._front
                for i in range(which):
                    current_node = current_node._next
                current_node._value = cosa

    def remove(self, which):
        ''' Removes the given node'''
        if which < 0 or which > self._nodes:
            print("Location out of bounds...")
        else:
            # If removing first...
            if which == 0:
                # the first and only !!
                if self._nodes == 1:
                    self._front = None
                    self._lastnode = None
                else:
                    self._front = self._front._next
            # If removng last...
            # elif: which+1 == self._nodes:
                # No double linked, so we need to iterate...

            # Removing in the midle
            else:
                current_node = self._front
                for i in range(which-1):
                    current_node = current_node._next
                # If current is last node...
                if current_node._next._next == None:
                    current_node._next = None
                    self._lastnode = current_node
                else:
                    current_node._next = current_node._next._next
            self._nodes -= 1

    def to_list(self):
        ''' Returns an ordered set with node (index, value) tuples
            Should be an iterator or generator...
        '''
        l = [(0, self._front._value)]
        current_node = self._front
        for i in range(self._nodes-1):
            current_node = current_node._next
            #k = (i, current_node._value)
            l.append((i+1, current_node._value))
        return l
