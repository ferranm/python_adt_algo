#!/usr/bin/python3

import fmc_adts as fmc

L = fmc.LinkedList()
print("Adding 'first', 'second' i 'third'")
L.add("")
L.add("second")
L.add("third")
print(L.to_list())

print("Setting values to indices")
L.set(0, "zero")
L.set(2, "second")
L.set(1, "first")
print(L.to_list())

print("Removing 'Front'")
L.remove(0)
print(L.to_list())

print("Inserting at '1,5' at index 1")
L.insert(1, "1,5")
print(L.to_list())

print("Removing idx 5 (outofbound...) and idx 2")
L.remove(5)
L.remove(2)
print(L.to_list())

print("Adding 'second' i 'third'")
L.add("second")
L.add("third")
print(L.to_list())


print("  *****    BY INDEX   ******  ")
print(L[2])

print("  *****    BY NEXT   ******  ")
print(next(L).get_value())
print(next(L).get_value())
print(next(L).get_value())
print(next(L).get_value())
print(next(L).get_value())
print(next(L).get_value())


print("  *****    ITERATING   ******  ")
print("There are %d nodes" % L.size())
for n in L:
    print(n)

print("  *****    ITERATING  2 ******  ")
print("There are %d nodes" % L.size())
for n in L:
    print(n)
